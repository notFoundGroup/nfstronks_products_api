package testUtils;

import com.notfound.nfstronks_products_api.dto.ProductDTO;
import com.notfound.nfstronks_products_api.models.Product;

import java.util.Date;

public class ProductsForTest {

    public static Product productOK(){
        return Product.builder()
                .author("Henrique Lima")
                .country("BR")
                .description("Obra boa")
                .price(100d)
                .launchDate(new Date())
                .title("ObraLinda").build();
    }

    public static Product productFULL(){
        return Product.builder()
                .author("Henrique Lima")
                .country("BR")
                .description("Obra boa")
                .price(100d)
                .launchDate(new Date())
                .title("ObraLinda")
                .isAvailable(false)
                .id(1L).build();
    }

    public static Product productWithAvailableTrue(){
        return Product.builder()
                .author("Henrique Lima")
                .country("BR")
                .description("Obra boa")
                .price(100d)
                .launchDate(new Date())
                .title("ObraLinda")
                .isAvailable(true).build();
    }

    public static ProductDTO productDTOFULL(){
        return ProductDTO.builder()
                .author("Henrique Lima")
                .country("BR")
                .description("Obra boa")
                .price(100d)
                .launchDate(new Date())
                .title("ObraLinda")
                .isAvailable(false).build();
    }

}
