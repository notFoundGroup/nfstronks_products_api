package com.notfound.nfstronks_products_api.unitTests;


import com.notfound.nfstronks_products_api.controllers.ProductController;
import com.notfound.nfstronks_products_api.exceptions.custom_exceptions.ProductNotFoundException;
import com.notfound.nfstronks_products_api.exceptions.custom_exceptions.TitleAlreadyExistsException;
import com.notfound.nfstronks_products_api.models.Product;
import com.notfound.nfstronks_products_api.services.interfaces.ProductService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.mockito.Mockito.times;

@ExtendWith(SpringExtension.class)
@DisplayName("ProductsController Unit Tests...")
class ProductControllerTest {

    @InjectMocks
    ProductController productController;

    @Mock
    ProductService productService;


    @Test
    @DisplayName("getAll Call service.getAll()")
    void getAll_callServiceGetAll_Always() {

        Pageable pageable = PageRequest.of(1, 1);

        ResponseEntity<Page<Product>> productPage = productController.getAll(pageable);

        Mockito.verify(productService, times(1)).getAll(pageable);
    }

    @Test
    @DisplayName("getByID Call service.getByID()")
    void getByID_callServiceGetByID_Always() throws ProductNotFoundException {

        ResponseEntity<Product> product = productController.getByID(1L);
        Mockito.verify(productService, times(1)).getByID(1L);
    }

    @Test
    @DisplayName("getAllAvailable Call service.getAllAvaible()")
    void getByID_callServiceGetAvailable_Always() throws ProductNotFoundException {

        Pageable pageable = PageRequest.of(1, 1);

        ResponseEntity<Page<Product>> productPage = productController.getAllAvailable(pageable);
        Mockito.verify(productService, times(1)).getAllAvailable(pageable);
    }

    @Test
    @DisplayName("getPriceBetween Call service.getByPriceBetween()")
    void getByID_callServiceGetByPriceBetween_Always() throws ProductNotFoundException {

        Pageable pageable = PageRequest.of(1, 1);

        ResponseEntity<Page<Product>> productPage = productController.getPriceBetween(1d,1d, pageable);
        Mockito.verify(productService, times(1)).getByPriceBetween(1d, 1d, pageable);
    }

    @Test
    @DisplayName("register Call service.Register()")
    void getByID_callServiceRegister_Always() throws ProductNotFoundException, IOException, TitleAlreadyExistsException {

        ResponseEntity<Product> product = productController.register(ArgumentMatchers.any(MultipartFile.class),
                                                                         ArgumentMatchers.any());

        Mockito.verify(productService, times(1)).register(ArgumentMatchers.any(),
                                                                                    ArgumentMatchers.any());
    }

    @Test
    @DisplayName("delete Call service.delete()")
    void delete_callServiceDelete_Always() throws ProductNotFoundException {

        productController.delete(1L);
        Mockito.verify(productService, times(1)).delete(1L);
    }

    @Test
    @DisplayName("makeUnavailable Call service.makeUnavailable()")
    void makeUnavailable_callServiceMakeUnavailable_Always() throws ProductNotFoundException {

        productController.makeUnavailable(1L);
        Mockito.verify(productService, times(1)).makeUnavailable(1L);
    }

}










