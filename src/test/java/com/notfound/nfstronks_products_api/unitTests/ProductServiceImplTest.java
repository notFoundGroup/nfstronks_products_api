package com.notfound.nfstronks_products_api.unitTests;

import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notfound.nfstronks_products_api.dto.ProductDTO;
import com.notfound.nfstronks_products_api.exceptions.custom_exceptions.ProductNotFoundException;
import com.notfound.nfstronks_products_api.exceptions.custom_exceptions.TitleAlreadyExistsException;
import com.notfound.nfstronks_products_api.models.Product;
import com.notfound.nfstronks_products_api.repositories.ProductRepository;
import com.notfound.nfstronks_products_api.services.impl.ProductServiceImpl;
import com.notfound.nfstronks_products_api.services.interfaces.S3;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

import static testUtils.ProductsForTest.productDTOFULL;
import static testUtils.ProductsForTest.productFULL;

@ExtendWith(SpringExtension.class)
@DisplayName("Products Unit Tests...")
class ProductServiceImplTest {

    @InjectMocks
    ProductServiceImpl productService;

    @Mock
    ProductRepository repositoryMocked;

    @Mock
    S3 s3;

    @BeforeEach
    void setUp() {
        Product productFULL = productFULL();
        Page<Product> page = Mockito.mock(Page.class);

        BDDMockito.when(repositoryMocked.findAll(ArgumentMatchers.isA(Pageable.class))).thenReturn(page);

        BDDMockito.when(repositoryMocked.findByPriceBetween(ArgumentMatchers.any(),
                ArgumentMatchers.any(),
                ArgumentMatchers.isA(Pageable.class))).thenReturn(page);

        BDDMockito.when(repositoryMocked.findByIsAvailableTrue(ArgumentMatchers.isA(Pageable.class))).thenReturn(page);

        BDDMockito.when(repositoryMocked.save(ArgumentMatchers.any())).thenReturn(productFULL);

        BDDMockito.when(repositoryMocked.findById(ArgumentMatchers.any())).thenReturn(Optional.of(productFULL));

        BDDMockito.when(repositoryMocked.findByTitle(ArgumentMatchers.any())).thenReturn(Optional.empty());
    }

    @Test
    @DisplayName("GetByID returns a Product When successful")
    void getByID_returnsProduct_whenSuccessful() throws ProductNotFoundException {
        Product productFULL = productFULL();

        Product productReturned = productService.getByID(1L);

        Assertions.assertThat(productReturned).isNotNull();
        Assertions.assertThat(productReturned.getTitle()).isEqualTo(productFULL.getTitle());
    }

    @Test
    @DisplayName("GetByID Throw ProductNotFoundExc When product was not found")
    void getByID_throwexception_whenProductnotFound() throws ProductNotFoundException {

        BDDMockito.when(repositoryMocked.findById(ArgumentMatchers.any())).thenReturn(Optional.empty());

        Assertions.assertThatThrownBy(() -> productService.getByID(1L)).isInstanceOf(ProductNotFoundException.class);
    }

    @Test
    @DisplayName("getAll Returns Page.class When successful")
    void getAll_returnsPageClass_whenSuccessful() {

        Pageable pageable = PageRequest.of(1, 1);

        Page<Product> produtosPage = productService.getAll(pageable);

        Assertions.assertThat(produtosPage).isInstanceOf(Page.class);
    }

    @Test
    @DisplayName("getAll Returns Page.class When successful")
    void getAllAvailable_returnsPageClass_whenSuccessful() {

        Pageable pageable = PageRequest.of(1, 1);

        Page<Product> produtosPage = productService.getAllAvailable(pageable);

        Assertions.assertThat(produtosPage).isInstanceOf(Page.class);
    }

    @Test
    @DisplayName("getAll Returns Page.class When successful")
    void getByPriceBetween_returnsPageClass_whenSuccessful() {

        Pageable pageable = PageRequest.of(1, 1);

        Page<Product> produtosPage = productService.getByPriceBetween(1d,2d, pageable);

        Assertions.assertThat(produtosPage).isInstanceOf(Page.class);
    }

    @Test
    @DisplayName("makeUnavailable Returns Product When successfull")
    void makeUnavailable_returnsProduct_whenSuccessful() throws ProductNotFoundException {

        Product productFULL = productFULL();

        Product productReturned = productService.makeUnavailable(1L);

        Mockito.verify(repositoryMocked, Mockito.times(1)).findById(1L);

        Assertions.assertThat(productReturned.getIsAvailable()).isFalse();
        Assertions.assertThat(productReturned.getTitle()).isEqualTo(productFULL.getTitle());
    }

    @Test
    @DisplayName("delete call repository.delete() When successfull")
    void delete_callRepositoryDelete_whenSuccessful() throws ProductNotFoundException {

        productService.delete(1L);

        Mockito.verify(repositoryMocked, Mockito.times(1)).deleteById(1L);
    }

    @Test
    @DisplayName("validateTitle call repository.FindByTitle() ALWAYS")
    void validateTittle_callRepositoryFindByTitle_always() throws TitleAlreadyExistsException {

        ProductDTO productDTOFULL = productDTOFULL();


        productService.validateTitle(productDTOFULL);

        Mockito.verify(repositoryMocked, Mockito.times(1)).findByTitle(productDTOFULL.getTitle());
    }

    @Test
    @DisplayName("validateTitle Throw TitleAlreadyExistsException When Fail")
    void validateTittle_ThrowTitleAlreadyExistsException_whenFail() {

        ProductDTO productDTOFULL = productDTOFULL();

        Product productFULL = productFULL();

        BDDMockito.when(repositoryMocked.findByTitle(ArgumentMatchers.any())).thenReturn(Optional.of(productFULL));

        Assertions.assertThatThrownBy(() -> productService.validateTitle(productDTOFULL))
                                                          .isInstanceOf(TitleAlreadyExistsException.class);

        Mockito.verify(repositoryMocked, Mockito.times(1)).findByTitle(productDTOFULL.getTitle());
    }

    @Test
    @DisplayName("register Returns Product When success")
    void register_returnsProduct_whenSuccessful() throws IOException, TitleAlreadyExistsException {

        ProductDTO productDTOFULL = productDTOFULL();

        String productDTOFULLJSON = new ObjectMapper().writeValueAsString(productDTOFULL);

        Product productRegistered = productService.register((productDTOFULLJSON), ArgumentMatchers.any(MultipartFile.class));

        Mockito.verify(repositoryMocked, Mockito.times(1))
                                                .findByTitle(productDTOFULL.getTitle());

        Mockito.verify(s3, Mockito.times(1))
                                  .saveMultiPartFile(
                                  ArgumentMatchers.any(),
                                  ArgumentMatchers.any(),
                                  ArgumentMatchers.any(String.class));

        Assertions.assertThat(productRegistered.getTitle()).isEqualTo(productDTOFULL.getTitle());
    }
}