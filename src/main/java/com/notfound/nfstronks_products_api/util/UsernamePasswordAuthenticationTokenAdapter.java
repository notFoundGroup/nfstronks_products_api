package com.notfound.nfstronks_products_api.util;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class UsernamePasswordAuthenticationTokenAdapter {
    Object principal;
    Object credentials;
    String[] rolesString;
}
