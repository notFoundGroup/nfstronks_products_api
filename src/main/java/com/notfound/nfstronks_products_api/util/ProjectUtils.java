package com.notfound.nfstronks_products_api.util;


import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.StringWriter;

@Slf4j
public class ProjectUtils {

    public static void stackTracingToLog(Exception e){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String stackTraceAsString = sw.toString();
        log.error(stackTraceAsString);
    }
}
