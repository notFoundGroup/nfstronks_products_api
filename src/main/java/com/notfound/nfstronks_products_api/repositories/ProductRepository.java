package com.notfound.nfstronks_products_api.repositories;


import com.notfound.nfstronks_products_api.models.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
    Page<Product> findAllByAuthor(String author, Pageable pageable);

    Page<Product> findAllByCountry(String country, Pageable pageable);

    Page<Product> findByPriceBetween(Double start, Double end, Pageable pageable);

    List<Product> findByPriceBetween(Double start, Double end);

    Optional<Product> findByTitle(String title);

    Page<Product> findByIsAvailableTrue(Pageable pageable);
}
