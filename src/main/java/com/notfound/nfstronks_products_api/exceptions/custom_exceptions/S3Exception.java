package com.notfound.nfstronks_products_api.exceptions.custom_exceptions;

public class S3Exception extends Exception{
    public S3Exception(String message) {
        super(message);
    }
}
