package com.notfound.nfstronks_products_api.exceptions.custom_exceptions;

public class ProductNotFoundException extends Exception {
    public ProductNotFoundException() {
        super("Produto não encontrado!");
    }
}
