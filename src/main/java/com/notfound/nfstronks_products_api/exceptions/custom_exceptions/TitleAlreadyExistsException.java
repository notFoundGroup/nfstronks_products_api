package com.notfound.nfstronks_products_api.exceptions.custom_exceptions;

public class TitleAlreadyExistsException extends Exception{

    public TitleAlreadyExistsException() {
        super("Esta obra já existe em nossa loja.");
    }
}
