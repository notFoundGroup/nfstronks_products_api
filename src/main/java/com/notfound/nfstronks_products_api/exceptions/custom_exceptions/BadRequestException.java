package com.notfound.nfstronks_products_api.exceptions.custom_exceptions;

public class BadRequestException extends RuntimeException {

    public BadRequestException(String message) {
        super(message);
    }
}
