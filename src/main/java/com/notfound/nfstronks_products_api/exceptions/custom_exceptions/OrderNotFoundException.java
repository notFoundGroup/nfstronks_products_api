package com.notfound.nfstronks_products_api.exceptions.custom_exceptions;

public class OrderNotFoundException extends Exception {

    public OrderNotFoundException() {
        super("Pedido não encontrado.");
    }

}