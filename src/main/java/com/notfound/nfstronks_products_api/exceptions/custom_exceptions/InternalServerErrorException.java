package com.notfound.nfstronks_products_api.exceptions.custom_exceptions;

public class InternalServerErrorException extends RuntimeException {

    public InternalServerErrorException(String message) {
        super(message);
    }
}
