package com.notfound.nfstronks_products_api.exceptions.custom_exceptions;

public class AuthException extends RuntimeException {

    public AuthException(String ErrorMessage) {
        super(ErrorMessage);
    }
}
