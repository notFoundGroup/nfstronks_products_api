package com.notfound.nfstronks_products_api.models;


import com.notfound.nfstronks_products_api.dto.ProductDTO;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tb_products")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @Column(name = "product_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_title", nullable = false, length = 100, unique = true)
    private String title;

    @Column(name = "product_author", nullable = false, length = 100)
    private String author;

    @Column(name = "product_description", nullable = false)
    private String description;

    @Column(name = "product_country", nullable = false, length = 70)
    private String country;

    @Column(name = "product_launch_date", nullable = false)
    private Date launchDate;

    @Column(name = "product_is_available", nullable = false)
    private Boolean isAvailable;

    @Column(name = "product_price", nullable = false)
    private Double price;

    public void update(ProductDTO newProduct) {
        this.title = newProduct.getTitle() != null ? newProduct.getTitle() : this.title;
        this.author = newProduct.getAuthor() != null ? newProduct.getAuthor() : this.author;
        this.description = newProduct.getDescription() != null ? newProduct.getDescription() : this.description;
        this.country = newProduct.getCountry() != null ? newProduct.getCountry() : this.country;
        this.launchDate = newProduct.getLaunchDate() != null ? newProduct.getLaunchDate() : this.launchDate;
        this.isAvailable = newProduct.getIsAvailable() != null ? newProduct.getIsAvailable() : this.isAvailable;
        this.price = newProduct.getPrice() != null ? newProduct.getPrice() : this.price;
    }
}
