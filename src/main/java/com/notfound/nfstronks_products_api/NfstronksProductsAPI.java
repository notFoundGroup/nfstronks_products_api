package com.notfound.nfstronks_products_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NfstronksProductsAPI {
    public static void main(String[] args) {
        SpringApplication.run(NfstronksProductsAPI.class, args);
    }
}
