package com.notfound.nfstronks_products_api.services.impl.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;

public class AWSCredentials {

    static com.amazonaws.auth.AWSCredentials awsCredentials =
            new BasicAWSCredentials(System.getenv("AWS_ACCESS_KEY"), System.getenv("AWS_SECRET_KEY"));


    static AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);

    public static AWSCredentialsProvider getAwsCredentialsProvider() {
        return awsCredentialsProvider;
    }

}
