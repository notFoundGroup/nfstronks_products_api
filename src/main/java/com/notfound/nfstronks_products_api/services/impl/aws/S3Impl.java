package com.notfound.nfstronks_products_api.services.impl.aws;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.notfound.nfstronks_products_api.services.interfaces.S3;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static com.notfound.nfstronks_products_api.services.impl.aws.AWSCredentials.awsCredentialsProvider;

@Service
@Primary
@Slf4j
public class S3Impl implements S3 {

    public final AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withCredentials(awsCredentialsProvider)
            .withRegion(Regions.US_EAST_1).build();


    public void saveMultiPartFile(MultipartFile multipartFile, String bucketName, String name) throws IOException {
        try {
            log.info("Start configuration to saving image: " + name + " in bucket: " + bucketName);
            ObjectMetadata data = new ObjectMetadata();
            log.debug("Getting content type");
            data.setContentType(multipartFile.getContentType());
            log.debug("Getting size");
            data.setContentLength(multipartFile.getSize());
            log.debug("Saving image:" + name + " in bucket:" + bucketName);
            s3Client.putObject(bucketName, name, multipartFile.getInputStream(), data);
            log.info("Image Saved.");
        } catch (Exception e){
            e.printStackTrace();
        }
    }



}
