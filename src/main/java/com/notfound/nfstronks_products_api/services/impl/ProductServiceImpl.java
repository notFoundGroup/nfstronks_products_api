package com.notfound.nfstronks_products_api.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notfound.nfstronks_products_api.dto.ProductDTO;
import com.notfound.nfstronks_products_api.exceptions.custom_exceptions.ProductNotFoundException;
import com.notfound.nfstronks_products_api.exceptions.custom_exceptions.TitleAlreadyExistsException;
import com.notfound.nfstronks_products_api.models.Product;
import com.notfound.nfstronks_products_api.repositories.ProductRepository;
import com.notfound.nfstronks_products_api.services.interfaces.ProductService;
import com.notfound.nfstronks_products_api.services.interfaces.S3;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@Service
@Slf4j
@Primary
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private final S3 s3;

    public ProductServiceImpl(ProductRepository productRepository, S3 s3) {
        this.productRepository = productRepository;
        this.s3 = s3;
    }

    @Override
    public Product getByID(Long id) throws ProductNotFoundException {
        Optional<Product> productOptional = productRepository.findById(id);

        if (productOptional.isEmpty()) {
            throw new ProductNotFoundException();
        }

        return productOptional.get();
    }

    @Override
    public Page<Product> getAll(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    @Override
    public Page<Product> getAllAvailable(Pageable pageable) {
        return productRepository.findByIsAvailableTrue(pageable);
    }

    @Override
    public Page<Product> getByPriceBetween(Double min, Double max, Pageable pageable) {
        return productRepository.findByPriceBetween(min, max, pageable);
    }

    @Override
    public Product register(String newProductJSON, MultipartFile nft) throws IOException, TitleAlreadyExistsException {

        log.info("Converting JsonProduct to Product");
        ProductDTO newProductDTO = new ObjectMapper().readValue(newProductJSON, ProductDTO.class);

        this.validateTitle(newProductDTO);

        s3.saveMultiPartFile(nft, System.getenv("AWS_BUCKET_PRODUCTS"), newProductDTO.getTitle());

        Product newProduct = new Product();
        newProduct.update(newProductDTO);

        return productRepository.save(newProduct);
    }

    @Override
    public void delete(Long id) throws ProductNotFoundException {
        productRepository.deleteById(id);
    }

    @Override
    public void validateTitle(ProductDTO product) throws TitleAlreadyExistsException {
        log.info("Verifying if the title are unique...");
        if (product.getTitle() != null) {
            Optional<Product> productOp = productRepository.findByTitle(product.getTitle());
            if (productOp.isPresent()) {
                log.error("Title is not unique.");
                throw new TitleAlreadyExistsException();
            }
        }
        log.info("Title is unique");
    }

    @Override
    public Product makeUnavailable(Long id) throws ProductNotFoundException {
        Product product = this.getByID(id);

        product.setIsAvailable(false);

        return productRepository.save(product);
    }


}