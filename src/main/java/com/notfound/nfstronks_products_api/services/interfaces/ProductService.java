package com.notfound.nfstronks_products_api.services.interfaces;


import com.notfound.nfstronks_products_api.dto.ProductDTO;
import com.notfound.nfstronks_products_api.exceptions.custom_exceptions.ProductNotFoundException;
import com.notfound.nfstronks_products_api.exceptions.custom_exceptions.TitleAlreadyExistsException;
import com.notfound.nfstronks_products_api.models.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public interface ProductService {
    Product getByID(Long id) throws ProductNotFoundException;

    Page<Product> getAll(Pageable pageable);

    Page<Product> getAllAvailable(Pageable pageable);

    Page<Product> getByPriceBetween(Double min, Double max, Pageable pageable);

    Product register(String newProductJSON, MultipartFile nft) throws IOException, TitleAlreadyExistsException;

    void delete(Long id) throws ProductNotFoundException;

    void validateTitle(ProductDTO product) throws TitleAlreadyExistsException;

    Product makeUnavailable(Long id) throws ProductNotFoundException;

}