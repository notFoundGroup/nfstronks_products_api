package com.notfound.nfstronks_products_api.services.interfaces;


import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface S3 {
    public void saveMultiPartFile(MultipartFile multipartFile, String bucketName, String name) throws IOException;
}
