package com.notfound.nfstronks_products_api.controllers;


import com.notfound.nfstronks_products_api.exceptions.custom_exceptions.BadRequestException;
import com.notfound.nfstronks_products_api.exceptions.custom_exceptions.InternalServerErrorException;
import com.notfound.nfstronks_products_api.exceptions.custom_exceptions.ProductNotFoundException;
import com.notfound.nfstronks_products_api.exceptions.custom_exceptions.TitleAlreadyExistsException;
import com.notfound.nfstronks_products_api.models.Product;
import com.notfound.nfstronks_products_api.services.interfaces.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static com.notfound.nfstronks_products_api.util.ProjectUtils.stackTracingToLog;


@RestController
@Slf4j
@RequestMapping("/products")
@CrossOrigin("*")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<Page<Product>> getAll(Pageable peageble) {
        try {
            return ResponseEntity.ok(productService.getAll(peageble));
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getByID(@PathVariable Long id) {
        try {

            return ResponseEntity.ok(productService.getByID(id));

        } catch (ProductNotFoundException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @GetMapping("/available")
    public ResponseEntity<Page<Product>> getAllAvailable(Pageable pageable){
        try{
            return ResponseEntity.ok(productService.getAllAvailable(pageable));

        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @GetMapping("/price")
    public ResponseEntity<Page<Product>> getPriceBetween(@RequestParam Double max, @RequestParam Double min, Pageable pageable) {
        try {

            return ResponseEntity.ok(productService.getByPriceBetween(min, max, pageable));

        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }


    @PostMapping
    public ResponseEntity<Product> register(@RequestParam("nft") MultipartFile nft, @RequestParam("productJson") String newProductJSON) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(productService.register(newProductJSON, nft));
        } catch (TitleAlreadyExistsException | IOException e) {
            throw new BadRequestException(e.getMessage());
        } catch (InternalServerErrorException e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        try {
            productService.delete(id);
        } catch (ProductNotFoundException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @PatchMapping("/makeUnavailable/{id}")
    public void makeUnavailable(@PathVariable Long id) {
        try {
            productService.makeUnavailable(id);
        } catch (ProductNotFoundException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            stackTracingToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }
}
