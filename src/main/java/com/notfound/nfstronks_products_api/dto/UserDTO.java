package com.notfound.nfstronks_products_api.dto;

import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Data
public class UserDTO {

    @NotEmpty
    private String name;

    @NotEmpty
    private Date birthDate;

    @NotEmpty
    @CPF
    private String cpf;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Min(11)
    @Max(11)
    private String phone;

    @NotEmpty
    private String role;
}
