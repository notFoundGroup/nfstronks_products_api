package com.notfound.nfstronks_products_api.dto;


import com.notfound.nfstronks_products_api.models.Product;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;


@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
public class ProductDTO {

    @NotEmpty
    private String title;

    @NotEmpty
    private String author;

    @NotEmpty
    private String description;

    @NotEmpty
    private String country;

    @NotNull
    private Date launchDate;

    @NotNull
    private Boolean isAvailable;

    @NotNull
    private Double price;

    public ProductDTO(Product product) {
        this.author = product.getAuthor();
        this.country = product.getCountry();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.isAvailable = product.getIsAvailable();
        this.launchDate = product.getLaunchDate();
        this.title = product.getTitle();
    }
}
